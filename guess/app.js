function checkGuess(guess) {
    var answers = ["red",
                   "green",
                   "blue"];

    var index = Math.floor(Math.random() * answers.length);

    if (guess == answers[index]) {
        answer = "You're right! I was thinking of " + answers[index];
    } else {
        answer = "Sorry, I was thinking of " + answers[index];
    }
    
    return answer;
}


function handleSendButton() {
    var guessInput = document.getElementById("guess");
    var guess = guessInput.value;
    var answer = null;

    answer = checkGuess(guess);
    alert(answer);
}

var sendButton = document.getElementById("sendButton");
sendButton.onclick = handleSendButton;




