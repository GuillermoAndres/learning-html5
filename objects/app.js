
// This is object literals
var fido = {
    name: "Fido",
    weight: 40,
    breed: "mixed",
    loves: ["walks", "fetching ball"],
    bark: function() {  //anonymous function
        alert("Woof woof");
    }
    
};

/*  Access object property */

// Dot notation
if (fido.weight > 25) {
    alert("Wooof");
} else {
    alert("yip");
}

// Brackets notation or string notation.

if (fido["breed"] == "mixed") {
    alert("This is the best");
}


/* Enumate all an object's properties */
var prop;
for (prop in fido) {
    console.log("Fido has a " + prop + " property");
    console.log(prop + ": " + fido[prop]);
}

/* To add a property to an object you simply assign a new property a
   value, like this: */

fido.age = 5;
console.log(fido.age);

/* Likewise, you can delete any property with the delete keyword,
   like this: */

delete fido.age;


// call method
fido.bark();

/* Objects created by a constructor are created by using new and
   a constructor function, which returns the object. */

// we'll create a constructor for create any dog, litles or many
function Dog(name, weight, breed) {
    this.name = name;
    this.weight = weight;
    this.breed = breed;
    this.bark = function() {
        if (this.weight > 25) {
            alert(this.name + " says woof!!");
        } else {
            alert(this.name + "says yip!");
        }
    };
}

// we'll crate some dogs
var firulais = new Dog("Firulaios", "Mixed", 65);
var cake = new Dog("Chaekpop", "Chawwaa", 12);
var zero = new Dog("Zero ", "Pure", 23 );

firulais.bark();
cake.bark();
zero.bark();
