/* Lo mejor que agrego javascript son los objects y las
 * functions, ahora imaginete unir estas dos grandes cosas */

// This is object literals
var movie1 = {
    title: "Plan 9 from Outer Space",
    genre: "cult classic",
    rating: 2,
    showtimes : ["1:00pm", "7:00pm", "11:00pm"],
    getNextShowing : function() {
        var now = new Date().getTime();
        for (var i = 0; i < this.showtimes.length; i++) {
            var showtime = getTimeFromString(this.showtimes[i]);
            if ((showtime - now) > 0) {
                return "Next showing of " + this.title + " is " + this.showtimes[i];
            }
        }
        return null;
    }
};

var movie2 = {
    title: "Forbidden Planet",
    genre: "classic sci-fi",
    rating: 5,
    showtimes : ["5:00pm", "9:00pm"]
};

console.log(movie1);
console.log(movie2);

/* Functions */
// function getNextShowing(movie) {
//     var now = new Date().getTime();
//     for (var i = 0; i < movie.showtimes.length; i++) {
//         var showtime = getTimeFromString(movie.showtimes[i]);
//         if ((showtime - now) > 0) {
//             return "Next showing of " + movie.title + " is " + movie.showtimes[i];
//         }
//     }
//     return null;
// }

function getTimeFromString(timeString) {
    var theTime = new Date();
    var time = timeString.match(/(\d+)(?::(\d\d))?\s*(p?)/); // => Array[ "3:30p", "3", "30", "p" ]
    theTime.setHours(parseInt(time[1]) + (time[3] ? 12 : 0) ); //  "" -> false "ads" -> true
    theTime.setMinutes(parseInt(time[2]) || 0);
    return theTime.getTime();
}


//var nextShowing = movie1.getNextShowing();
//alert(nextShowing);

// nextShowing = getNextShowing(movie2);
// alert(nextShowing);


// Usign OOP (Object Oriented Programming)
// Now, we'll create contructor for create any movies

function Movie(title, genre, rating, showtimes) {
    this.title = title;
    this.genre = genre;
    this.rating = rating;
    this.showtimes = showtimes;
    this.getNextShowing = function() {
        var now = new Date().getTime();
        for (var i = 0; i < this.showtimes.length; i++) {
            var showtime = getTimeFromString(this.showtimes[i]);
            if ((showtime - now) > 0) {
                return "Next showing of " + this.title + " is " + this.showtimes[i];
            }
        }
        return null;
    };
}


var banzaiMovie = new Movie("Buckaroo Banzai",
                            "Cult Classic",
                            5,
                            ["1:00pm", "5:00pm", "7:00pm", "11:00pm"]);

var plan9Movie = new Movie("Plan 9 from Outer Space",
                           "Cult Classic",
                           2,
                           ["3:00pm", "7:00pm", "11:00pm"]);

var forbiddenPlanetMovie = new Movie("Forbidden Planet",
                                     "Classic Sci-fi",
                                     5,
                                     ["5:00pm", "9:00pm"]);


alert(banzaiMovie.getNextShowing());
alert(plan9Movie.getNextShowing());
alert(forbiddenPlanetMovie.getNextShowing());
