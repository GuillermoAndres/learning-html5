function init() {
    // we've got a button and we've got a fuction
    var button = document.getElementById("addButton");
    // set up a handler
    button.onclick = handleButtonClick;
    loadPlaylist();
}

// It will act as a handler
function handleButtonClick() {
    // alert("Button was cliked!");
    var textInput = document.getElementById("songTextInput");
    var songName = textInput.value;
    if (songName == "") {
        alert("This is empty string");
        return;
    }
    // alert("Adding " + songName);
    var li = document.createElement("li");
    li.innerHTML = songName;
    var ul = document.getElementById("playlist");
    ul.appendChild(li);
    save(songName);
}

window.onload = init;
