

function init() {
    var sendButton = document.getElementById("sendButton");
    sendButton.onclick = handleButtonClick;
}

function handleButtonClick () {
    var usernameInput = document.getElementById("username");
    var passwordInput = document.getElementById("password");
    if (usernameInput.value == "" && passwordInput.value == "") {
        alert("It's empty string");
        return;
    }
    var username = usernameInput.value;
    var password = passwordInput.value;

    var newHeader = document.createElement("h1");
    newHeader.innerHTML = "Profile user";    
    var container = document.getElementsByClassName("container")[0];
    container.appendChild(newHeader);
    var usernameHeader = document.createElement("h3");
    usernameHeader.innerHTML = username;
    var passwordHeader = document.createElement("h3");
    passwordHeader.innerHTML = password;
    container.appendChild(usernameHeader);
    container.appendChild(passwordHeader);
}

window.onload = init;
