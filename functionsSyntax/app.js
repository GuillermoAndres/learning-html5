/*  Overviell function
    - Function declaration
    - Function expression
    - Function arrow
    - Immediately Invoked Function Expression (IIFE)
 */

/** Es el primer lenguaje que conozco que puede hacer que una function pude ser asignada a una variable**/

// Function expression

/* Scope limited: can't access lexical declaration 'greeting' before
 initialization */

// greeting("Guillermo"); // Reference error

// Function expression
let greeting = function(name) {
    console.log("Saludo " + name);
};

greeting("Guillermo"); // This is its scope

// We can also assigned again because is let lik variable
greeting = 2+2;
console.log(greeting);

// -------------------------------

// Function declaration
/* Function declarations are not part of the regular top-to-bottom flow
 of control. They are conceptually moved to the top of their scope and
 can be used by all the code in that scope.*/

farewell("Guillermo");

function farewell(name) {
    console.log(`Good bye ${name}`);
}


// ------------------------------

// Immediately Invoked Function Expression (IIFE)
// Scope only here
(function(firsname, lasname, hours){
    console.log(`Hello! ${firsname} ${lasname} is at ${hours}`);
}("Guillermo", "Andres", 7));

// ------------------------------

// Arrow function
/* Instead of the function keyword, it uses an arrow (=>) made up of an equal
 * sign and a greater-than character */

const power = (base, exp) => {
    let result = 1;
    for(let i = 0; i < exp; i++) {
        result *= base;
    }
    return result;
};

console.log(power(2, 5)); // 32

/* When there is only one parameter name, you can omit the parentheses around
 * the parameter list */

let square = x => {return x * x;};
console.log(square(5)); // 25;

/*If the body is a single expression, rather than a block in braces, that
 * expression will be returned from the function.*/

square = x => x * x;

console.log(square(6)); // 36

/*When an arrow function has no parameters at all, its parameter list is just an
 * empty set of parentheses*/
const horn = () => {
  console.log("Toot");
};

// typeof power  ; function
// ------------------------------
// var and let
// El interprete de javascript analiza primer las funciones y las variables var
// y las coloca en top del archivo, por eso se puede colocar en cualquier lado
//
// Para let, el scope de define en lugar donde se define por no puede usarse mas 
// atras.
console.log(desk);
console.log(book);

function goo() {
  console.log(name);
}

function foo() {
  var name = "Guillermo";
  let lastname = " Andres";
  // console.log(name);
  // console.log(lastname);
  goo();
}

var desk = "gnu/linux";
// let book = "history"; can't access lexical declaration 'book' before initialization
var book = "history";



